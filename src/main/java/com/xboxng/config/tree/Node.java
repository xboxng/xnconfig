package com.xboxng.config.tree;

import com.xboxng.config.util.ArrayUtils;

import java.util.LinkedList;
import java.util.List;

/**
 * User: qiang
 * Date: 6/5/12
 * Time: 7:24 PM
 */
class NodeSearchResult {
    Node nodeFound = null; // the actual node matched
    Node deepestMatch = null; // deepest match found
    int depth = 0;     // the depth of bestMatch
}

public final class Node {
    public static final String WILD_CARD = "*";
    public static final String PATH_DELIMITER = "\\.";
    private String name;
    private List<Node> children;
    private int firstWildCardNode = -1; //place of the first wildcard node
    private Object value;

    public Node() {
        this(null);
    }

    public Node(String name) {
        this.name = name;
    }

    private static void searchHelp(NodeSearchResult result, Node node, String[] path, int i, int j, boolean exactMatched) {
        if (!node.match(path[i])) {
            return;
        } else {
            // node is a potential match of path

            if (exactMatched && (!node.name.equals(WILD_CARD) || path[i].equals(WILD_CARD))) {
                // exactly matched so far, update the depth and deepestMatch of the result to be returned
                result.depth++;
                result.deepestMatch = node;
            } else {
                // not an exact match, future matches, if any, should not update the depth and the deepestMatch
                // of the result to be returned
                exactMatched = false;
            }

            if (i == j) {
                if (node.children == null) {
                    result.nodeFound = node;
                }
            } else {
                if (node.children != null) {
                    for (Node child : node.children) {
                        searchHelp(result, child, path, i + 1, j, exactMatched);
                        if (result.nodeFound != null)
                            return;
                    }
                }
            }
        }
    }

    private static String toString(Node node) {
        StringBuilder sb = new StringBuilder(node.name);
        if (node.children != null) {
            sb.append(" [ ");
            boolean first = true;
            for (Node c : node.children) {
                if (first) {
                    sb.append(toString(c));
                    first = false;
                } else {
                    sb.append(" , ");
                    sb.append(toString(c));
                }
            }
            sb.append(" ] ");
        } else {
            sb.append(":");
            sb.append(node.value);
        }
        return sb.toString();
    }

    private NodeSearchResult seachInternal(String[] path) {
        NodeSearchResult nsr = new NodeSearchResult();
        if (children != null) {
            for (Node child : children) {
                searchHelp(nsr, child, path, 0, path.length - 1, true);
                if (nsr.nodeFound != null) {
                    break;
                }
            }
        }
        return nsr;
    }

    protected Object getValue(String[] profile) {
        ArrayUtils.reverse(profile);
        NodeSearchResult nsr = seachInternal(profile);
        if (nsr.nodeFound != null)
            return nsr.nodeFound.value;
        else
            return null;
    }

    public Object getValue(String path) {
        String[] pe = path.split(PATH_DELIMITER);
        return getValue(pe);
    }

    private void addChild(Node node) {
        if (children == null) children = new LinkedList<Node>();
        if (node.name.equals(WILD_CARD)) {
            children.add(firstWildCardNode + 1, node);
        } else {
            children.add(0, node);
            firstWildCardNode++;
        }
    }

    protected void insertNode(String[] profile, Object value) {
        ArrayUtils.reverse(profile);
        NodeSearchResult nsr = seachInternal(profile);
        if (nsr.nodeFound != null && nsr.deepestMatch == nsr.nodeFound) {
            // the exact path is already in the tree
            // so we only need to update its value
            nsr.nodeFound.value = value;
        } else {
            // the path is new
            // get the root of the new path
            Node parent = nsr.deepestMatch;
            if (parent == null) {
                parent = this;
            }

            // create nodes for the path
            Node node = new Node();
            node.name = profile[nsr.depth];
            Node current = node;
            for (int i = nsr.depth + 1; i < profile.length; i++) {
                Node c = new Node();
                c.name = profile[i];
                current.addChild(c);
                current = c;
            }

            current.value = value;
            parent.addChild(node);
        }
    }

    public void insertNode(String profile, Object value) {
        String[] pe = profile.split(PATH_DELIMITER);
        this.insertNode(pe, value);
    }

    public boolean match(String name) {
        return this.name.equals(WILD_CARD) ? true : this.name.equals(name);
    }

    public String toString() {
        return toString(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
