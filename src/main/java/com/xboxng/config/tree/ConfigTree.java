/*
 *  Copyright Qiang Yu (qiangyu #### gmail.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.xboxng.config.tree;

import com.xboxng.config.exception.XNConfigException;
import com.xboxng.config.util.ArrayUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ConfigTree {
    private Map<String, Node> subTrees = new HashMap<String, Node>();
    private int profileLen = 1;

    public ConfigTree() {
    }

    public ConfigTree(int profileLen) {
        this.profileLen = profileLen;
    }

    public void insertNode(String path, Object value) throws XNConfigException {
        String[] pe = path.split(Node.PATH_DELIMITER, profileLen + 1);
        if (pe.length <= profileLen)
            throw new XNConfigException("the path (" + path + ") should have a prefix of length " + profileLen);
        Node root = subTrees.get(pe[profileLen]);

        if (root == null) {
            root = new Node(pe[profileLen]);
            subTrees.put(pe[profileLen], root);
        }

        root.insertNode((String[]) ArrayUtils.subarray(pe, 0, profileLen), value);
    }

    public Object getValue(String path) {
        String[] pe = path.split(Node.PATH_DELIMITER, profileLen + 1);
        Node root = subTrees.get(pe[profileLen]);

        if (root != null) {
            return root.getValue((String[]) ArrayUtils.subarray(pe, 0, profileLen));
        } else
            return null;
    }

    protected Map<String, Node> subTrees() {
        return this.subTrees;
    }

    public int getProfileLen() {
        return profileLen;
    }

    public void setProfileLen(int profileLen) {
        this.profileLen = profileLen;
    }

    public Set<String> allEntries() {
        return subTrees().keySet();
    }
}
