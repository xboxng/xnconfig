/*
 *  Copyright Qiang Yu (qiangyu #### gmail.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.xboxng.config.tree;

import java.util.HashMap;
import java.util.Map;

public class ConfigMap implements Resolvable {
    private Map<String, Object> elements = new HashMap<String, Object>();

    // all possible references in the map
    private Map<String, Resolvable> references = new HashMap<String, Resolvable>();

    /**
     * add a new value into the configuration map
     *
     * @param key
     * @param value
     * @return
     */
    public Object put(String key, Object value) {
        Object rv = elements.put(key, value);

        // if the new value is a reference, record it for future resolution (see @ReferenceResolver).
        if (value instanceof Resolvable) {
            references.put(key, (Resolvable) value);
        }
        return rv;
    }

    public Map<String, Resolvable> getReferences() {
        return references;
    }

    public Map<String, Object> getElements() {
        return elements;
    }
}
