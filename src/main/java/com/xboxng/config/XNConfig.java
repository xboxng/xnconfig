/*
 *  Copyright Qiang Yu (qiangyu #### gmail.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.xboxng.config;

import com.xboxng.config.exception.XNConfigException;
import com.xboxng.config.grammar.PLexer;
import com.xboxng.config.grammar.PParser;
import com.xboxng.config.tree.ConfigTree;
import com.xboxng.config.tree.Reference;
import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XNConfig {
    private static Pattern KEY_PATTERN = Pattern.compile("((_|[a-zA-Z])+\\w*)(\\.(_|[a-zA-Z])+\\w*)*");

    private ConfigTree tree;
    private ProfileResolver profileResolver;
    private ReferenceResolver referenceResolver;

    public XNConfig(InputStream input) throws IOException, XNConfigException {
        PLexer lex = new PLexer(new ANTLRInputStream(input));
        CommonTokenStream tokens = new CommonTokenStream(lex);

        PParser g = new PParser(tokens);
        try {
            g.start();
        } catch (RecognitionException e) {
            throw new XNConfigException(e);
        }

        tree = g.getConfig();
        referenceResolver = new ReferenceResolver(this);
    }

    public Object getValue(String key) {
        return getValue(key, new HashSet<Reference>());
    }

    protected Object getValue(String key, Set<Reference> pendingReferences) {
        Matcher matcher = KEY_PATTERN.matcher(key);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("invalid key:" + key);
        }
        StringBuilder completeKey = new StringBuilder(profileResolver.getProfile());
        completeKey.append(".");
        completeKey.append(key);
        return referenceResolver.resolve(tree.getValue(completeKey.toString()), pendingReferences);
    }

    public Long getLongValue(String key) {
        return (Long) this.getValue(key);
    }

    public Integer getIntegerValue(String key) {
        return Integer.parseInt(this.getValue(key).toString());
    }

    public Map getMapValue(String key) {
        return (Map) this.getValue(key);
    }

    public List getListValue(String key) {
        return (List) this.getValue(key);
    }

    public String getStringValue(String key) {
        return (String) this.getValue(key);
    }

    public ProfileResolver getProfileResolver() {
        return profileResolver;
    }

    public void setProfileResolver(ProfileResolver profileResolver) {
        this.profileResolver = profileResolver;
    }

    public Set<String> allEntries() {
        return tree.allEntries();
    }
}
