grammar P;

@header{
package com.xboxng.config.grammar;
import com.xboxng.config.tree.Reference;
import com.xboxng.config.tree.ConfigMap;
import com.xboxng.config.tree.ConfigList;
import com.xboxng.config.tree.ConfigTree;
import com.xboxng.config.exception.XNConfigException;
}

@lexer::header{
package com.xboxng.config.grammar;
import com.xboxng.config.exception.XNConfigException;
}

@rulecatch {
	catch (RecognitionException e) {
		throw new XNConfigException(getErrorHeader(e)+" "+getErrorMessage(e, PParser.tokenNames), e);
	}
}


@members{
	private ConfigTree config = new ConfigTree();
	
	public ConfigTree getConfig(){
		return config;
	}	

	@Override
	protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
		throw new MismatchedTokenException(ttype, input);
	}

	@Override
	public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow) throws RecognitionException {
		throw e;
	}
}

start	: config? assignment* EOF
	;

assignment
	: entity '=' value ';' { config.insertNode($entity.result, $value.result); } 
	;	

config	: '!' Prefix '=' number ';' { config.setProfileLen($number.result.intValue()); }
	;

value	returns [Object result]
	: string { $result = $string.result; }
	| number { $result = $number.result; }
	| bool { $result = $bool.result; }
	| map { $result = $map.result; }
	| list { $result = $list.result; }
	| reference { $result = $reference.result; }
	;


string 	returns [String result]
	: String { $result = $String.text; $result=$result.substring(1, $result.length()-1); }
	;

number	returns [Long result]
	: Number { $result = Long.valueOf($Number.text); }
	;

bool	returns [Boolean result]
	: Bool { $result = Boolean.valueOf($Bool.text); }
	;

map	returns [ConfigMap result]
@init { $result = new ConfigMap(); }
	: '{' members[$result]? '}' 
	;

list	returns [ConfigList result]
@init { $result = new ConfigList(); }
	: '[' elements[$result]? ']'
	;

elements[ConfigList list]
	: v1=value { list.add($v1.result); } (',' v2=value { list.add($v2.result); } )*
	;

members	[ConfigMap map]
	: pair[$map](',' pair[$map])*
	;

pair	[ConfigMap map]
	: property ':' value { $map.put($property.result, $value.result); }
	;
	 
entity	returns [String result]
	: { input.LT(2).getText().equals("=") }? KEY { $result=$KEY.text; }
	;
	
property
	returns [String result]
	: { !input.LT(1).getText().contains(".") && input.LT(2).getText().equals(":") }?  KEY { $result=$KEY.text; } 
	;	

reference 
	returns [Reference result]
@init { $result = new Reference(); }	
	: Reference { $result.setName($Reference.text.substring(1)); }
	;
		
Number	: '-'? Digit+
	;		

Bool	: 'true'|'false'
	;

Prefix	: 'PREFIX_NUM'
	;

String	: '"' ( EscapeSequence | ~('\u0000'..'\u001f' | '\\' | '\"' ))* '"'
     	;

WS	: (' '|'\n'|'\r'|'\t')+ { $channel=HIDDEN; }
    	; 


Comment	: '#' .* ('\n'|'\r') {$channel=HIDDEN;}
	| '/*' .* '*/' {$channel=HIDDEN;}
    	;
   
Reference
	:  '@' ID ('.' ID)*
	;	   
    	  
KEY	:  ('*' '.')* ID ('.' ID)*
	;    	  
    	      	  
fragment ID
	: ('_'|Letter) ('_'|Letter|Digit)*
	;
    	  	
fragment EscapeSequence
	:   '\\' (UnicodeEscape |'b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\'|'\/')
	;

fragment UnicodeEscape
	: 'u' HexDigit HexDigit HexDigit HexDigit
	;

fragment HexDigit
	: '0'..'9' | 'A'..'F' | 'a'..'f'
	;

fragment Digit
	: '0'..'9'
	;

fragment Letter
	: 'a'..'z' | 'A'..'Z'
	;