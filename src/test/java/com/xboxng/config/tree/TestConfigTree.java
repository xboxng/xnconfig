/*
 *  Copyright Qiang Yu (qiangyu #### gmail.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.xboxng.config.tree;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestConfigTree {
    @Test
    public void testTreeWithPofileLen2() {
        ConfigTree tree = new ConfigTree(2);
        tree.insertNode("*.*.jdbc.url", "jdbc://global.jdbc.com");
        tree.insertNode("*.*.jdbc.username", "global_username");
        tree.insertNode("*.*.jdbc.password", "global_password");

        //default
        tree.insertNode("*.xnconfig.jdbc.url", "jdbc://default.jdbc.com");
        tree.insertNode("*.xnconfig.jdbc.username", "default_username");
        tree.insertNode("*.xnconfig.jdbc.password", "default_password");

        // desktop
        tree.insertNode("desktop.xnconfig.jdbc.url", "jdbc://desktop.jdbc.com");
        tree.insertNode("desktop.xnconfig.jdbc.username", "desktop_username");
        tree.insertNode("desktop.xnconfig.jdbc.password", "desktop_password");

        // dev
        tree.insertNode("dev.xnconfig.jdbc.url", "jdbc://dev.jdbc.com");
        tree.insertNode("dev.xnconfig.jdbc.username", "dev_username");
        tree.insertNode("dev.xnconfig.jdbc.password", "dev_password");

        // prod
        tree.insertNode("prod.xnconfig.jdbc.url", "jdbc://prod.jdbc.com");
        tree.insertNode("prod.xnconfig.jdbc.username", "prod_username");
        tree.insertNode("prod.xnconfig.jdbc.password", "prod_password");

        assertEquals("jdbc://global.jdbc.com", tree.getValue("prod.another.jdbc.url"));
        assertEquals("global_username", tree.getValue("prod.another.jdbc.username"));
        assertEquals("global_password", tree.getValue("prod.another.jdbc.password"));

        assertEquals("jdbc://default.jdbc.com", tree.getValue("private.xnconfig.jdbc.url"));
        assertEquals("default_username", tree.getValue("private.xnconfig.jdbc.username"));
        assertEquals("default_password", tree.getValue("private.xnconfig.jdbc.password"));

        assertEquals("jdbc://desktop.jdbc.com", tree.getValue("desktop.xnconfig.jdbc.url"));
        assertEquals("desktop_username", tree.getValue("desktop.xnconfig.jdbc.username"));
        assertEquals("desktop_password", tree.getValue("desktop.xnconfig.jdbc.password"));

        assertEquals("jdbc://dev.jdbc.com", tree.getValue("dev.xnconfig.jdbc.url"));
        assertEquals("dev_username", tree.getValue("dev.xnconfig.jdbc.username"));
        assertEquals("dev_password", tree.getValue("dev.xnconfig.jdbc.password"));

        assertEquals("jdbc://prod.jdbc.com", tree.getValue("prod.xnconfig.jdbc.url"));
        assertEquals("prod_username", tree.getValue("prod.xnconfig.jdbc.username"));
        assertEquals("prod_password", tree.getValue("prod.xnconfig.jdbc.password"));

        // fix jdbc com.xboxng.config
        tree.insertNode("prod.xnconfig.jdbc.url", "prod_jdbc");
        tree.insertNode("dev.xnconfig.jdbc.url", "dev_jdbc");
        tree.insertNode("*.xnconfig.jdbc.url", "default_jdbc");
        tree.insertNode("*.*.jdbc.url", "global_jdbc");
        tree.insertNode("desktop.xnconfig.jdbc.url", "desktop_jdbc");

        // check values again
        assertEquals("global_jdbc", tree.getValue("prod.another.jdbc.url"));
        assertEquals("global_username", tree.getValue("prod.another.jdbc.username"));
        assertEquals("global_password", tree.getValue("prod.another.jdbc.password"));

        assertEquals("default_jdbc", tree.getValue("private.xnconfig.jdbc.url"));
        assertEquals("default_username", tree.getValue("private.xnconfig.jdbc.username"));
        assertEquals("default_password", tree.getValue("private.xnconfig.jdbc.password"));

        assertEquals("desktop_jdbc", tree.getValue("desktop.xnconfig.jdbc.url"));
        assertEquals("desktop_username", tree.getValue("desktop.xnconfig.jdbc.username"));
        assertEquals("desktop_password", tree.getValue("desktop.xnconfig.jdbc.password"));

        assertEquals("dev_jdbc", tree.getValue("dev.xnconfig.jdbc.url"));
        assertEquals("dev_username", tree.getValue("dev.xnconfig.jdbc.username"));
        assertEquals("dev_password", tree.getValue("dev.xnconfig.jdbc.password"));

        assertEquals("prod_jdbc", tree.getValue("prod.xnconfig.jdbc.url"));
        assertEquals("prod_username", tree.getValue("prod.xnconfig.jdbc.username"));
        assertEquals("prod_password", tree.getValue("prod.xnconfig.jdbc.password"));
    }

    @Test
    public void testTreeWithPofileLen1() {
        ConfigTree tree = new ConfigTree(1);

        //default
        tree.insertNode("*.jdbc.url", "default_jdbc");
        tree.insertNode("*.jdbc.username", "default_username");
        tree.insertNode("*.jdbc.password", "default_password");

        //dev
        tree.insertNode("dev.jdbc.url", "dev_jdbc");
        tree.insertNode("dev.jdbc.username", "dev_username");
        tree.insertNode("dev.jdbc.password", "dev_password");

        //prod
        tree.insertNode("prod.jdbc.url", "prod_jdbc");
        tree.insertNode("prod.jdbc.username", "prod_username");
        tree.insertNode("prod.jdbc.password", "prod_password");

        assertEquals("default_jdbc", tree.getValue("private.jdbc.url"));
        assertEquals("default_username", tree.getValue("private.jdbc.username"));
        assertEquals("default_password", tree.getValue("private.jdbc.password"));

        assertEquals("default_jdbc", tree.getValue("desktop.jdbc.url"));
        assertEquals("default_username", tree.getValue("desktop.jdbc.username"));
        assertEquals("default_password", tree.getValue("desktop.jdbc.password"));

        assertEquals("dev_jdbc", tree.getValue("dev.jdbc.url"));
        assertEquals("dev_username", tree.getValue("dev.jdbc.username"));
        assertEquals("dev_password", tree.getValue("dev.jdbc.password"));

        assertEquals("prod_jdbc", tree.getValue("prod.jdbc.url"));
        assertEquals("prod_username", tree.getValue("prod.jdbc.username"));
        assertEquals("prod_password", tree.getValue("prod.jdbc.password"));
    }

    @Test
    public void testNumberOfSubTrees() {
        ConfigTree tree = new ConfigTree(1);

        //default
        tree.insertNode("*.jdbc.url", "default_jdbc");
        tree.insertNode("*.jdbc.username", "default_username");
        tree.insertNode("*.jdbc.password", "default_password");

        //dev
        tree.insertNode("dev.jdbc.url", "dev_jdbc");
        tree.insertNode("dev.jdbc.username", "dev_username");
        tree.insertNode("dev.jdbc.password", "dev_password");

        //prod
        tree.insertNode("prod.jdbc.url", "prod_jdbc");
        tree.insertNode("prod.jdbc.username", "prod_username");
        tree.insertNode("prod.jdbc.password", "prod_password");

        assertEquals(3, tree.subTrees().keySet().size());

        tree.insertNode("jdbc.url", "incorrect_config");
        assertEquals(4, tree.subTrees().keySet().size());
    }
}
