/*
 *  Copyright Qiang Yu (qiangyu #### gmail.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.xboxng.config.tree;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestNode {
    @Test
    public void testInsertNode() {
        Node root = new Node();
        root.setName("root");
        // global configuration
        root.insertNode("*.*.jdbc.url", "jdbc://global.jdbc.com");
        root.insertNode("*.*.jdbc.username", "global_username");
        root.insertNode("*.*.jdbc.password", "global_password");

        //default
        root.insertNode("*.xnconfig.jdbc.url", "jdbc://default.jdbc.com");
        root.insertNode("*.xnconfig.jdbc.username", "default_username");
        root.insertNode("*.xnconfig.jdbc.password", "default_password");

        // desktop
        root.insertNode("desktop.xnconfig.jdbc.url", "jdbc://desktop.jdbc.com");
        root.insertNode("desktop.xnconfig.jdbc.username", "desktop_username");
        root.insertNode("desktop.xnconfig.jdbc.password", "desktop_password");

        // dev
        root.insertNode("dev.xnconfig.jdbc.url", "jdbc://dev.jdbc.com");
        root.insertNode("dev.xnconfig.jdbc.username", "dev_username");
        root.insertNode("dev.xnconfig.jdbc.password", "dev_password");

        // prod
        root.insertNode("prod.xnconfig.jdbc.url", "jdbc://prod.jdbc.com");
        root.insertNode("prod.xnconfig.jdbc.username", "prod_username");
        root.insertNode("prod.xnconfig.jdbc.password", "prod_password");

        System.out.println(root);

        // check values
        assertEquals("jdbc://global.jdbc.com", root.getValue("prod.another.jdbc.url"));
        assertEquals("global_username", root.getValue("prod.another.jdbc.username"));
        assertEquals("global_password", root.getValue("prod.another.jdbc.password"));

        assertEquals("jdbc://default.jdbc.com", root.getValue("private.xnconfig.jdbc.url"));
        assertEquals("default_username", root.getValue("private.xnconfig.jdbc.username"));
        assertEquals("default_password", root.getValue("private.xnconfig.jdbc.password"));

        assertEquals("jdbc://desktop.jdbc.com", root.getValue("desktop.xnconfig.jdbc.url"));
        assertEquals("desktop_username", root.getValue("desktop.xnconfig.jdbc.username"));
        assertEquals("desktop_password", root.getValue("desktop.xnconfig.jdbc.password"));

        assertEquals("jdbc://dev.jdbc.com", root.getValue("dev.xnconfig.jdbc.url"));
        assertEquals("dev_username", root.getValue("dev.xnconfig.jdbc.username"));
        assertEquals("dev_password", root.getValue("dev.xnconfig.jdbc.password"));

        assertEquals("jdbc://prod.jdbc.com", root.getValue("prod.xnconfig.jdbc.url"));
        assertEquals("prod_username", root.getValue("prod.xnconfig.jdbc.username"));
        assertEquals("prod_password", root.getValue("prod.xnconfig.jdbc.password"));

        // fix jdbc com.xboxng.config
        root.insertNode("prod.xnconfig.jdbc.url", "prod_jdbc");
        root.insertNode("dev.xnconfig.jdbc.url", "dev_jdbc");
        root.insertNode("*.xnconfig.jdbc.url", "default_jdbc");
        root.insertNode("*.*.jdbc.url", "global_jdbc");
        root.insertNode("desktop.xnconfig.jdbc.url", "desktop_jdbc");

        // check values again
        assertEquals("global_jdbc", root.getValue("prod.another.jdbc.url"));
        assertEquals("global_username", root.getValue("prod.another.jdbc.username"));
        assertEquals("global_password", root.getValue("prod.another.jdbc.password"));

        assertEquals("default_jdbc", root.getValue("private.xnconfig.jdbc.url"));
        assertEquals("default_username", root.getValue("private.xnconfig.jdbc.username"));
        assertEquals("default_password", root.getValue("private.xnconfig.jdbc.password"));

        assertEquals("desktop_jdbc", root.getValue("desktop.xnconfig.jdbc.url"));
        assertEquals("desktop_username", root.getValue("desktop.xnconfig.jdbc.username"));
        assertEquals("desktop_password", root.getValue("desktop.xnconfig.jdbc.password"));

        assertEquals("dev_jdbc", root.getValue("dev.xnconfig.jdbc.url"));
        assertEquals("dev_username", root.getValue("dev.xnconfig.jdbc.username"));
        assertEquals("dev_password", root.getValue("dev.xnconfig.jdbc.password"));

        assertEquals("prod_jdbc", root.getValue("prod.xnconfig.jdbc.url"));
        assertEquals("prod_username", root.getValue("prod.xnconfig.jdbc.username"));
        assertEquals("prod_password", root.getValue("prod.xnconfig.jdbc.password"));


        root.insertNode("*.user.retrycount", "2");
        assertEquals(null, root.getValue("prod.xboxng.user.retrycount"));
        assertEquals("2", root.getValue("prod.user.retrycount"));
        root.insertNode("*.*.user.retrycount", "3");
        assertEquals("3", root.getValue("prod.xboxng.user.retrycount"));

        System.out.println(root);
    }
}
