/*
 *  Copyright Qiang Yu (qiangyu #### gmail.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.xboxng.config.grammar;

import com.xboxng.config.tree.ConfigTree;
import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class TestParser {
    @Test
    public void testHappyCase() throws IOException, RecognitionException {
        PLexer lex = new PLexer(new ANTLRInputStream(TestParser.class.getResourceAsStream("/test_input.properties")));
        CommonTokenStream tokens = new CommonTokenStream(lex);

        PParser g = new PParser(tokens);
        g.start();

        ConfigTree config = g.getConfig();
        assertFalse((Boolean) config.getValue("dev.ssl"));
    }


    @Test
    public void testPrefixLen1() throws IOException, RecognitionException {
        PLexer lex = new PLexer(new ANTLRInputStream(TestParser.class.getResourceAsStream("/test_input_prefix_1.properties")));
        CommonTokenStream tokens = new CommonTokenStream(lex);

        PParser g = new PParser(tokens);
        g.start();

        ConfigTree config = g.getConfig();


        assertEquals("A", config.getValue("dev.message"));
        assertEquals("A", config.getValue("*.message"));
        assertEquals("A", config.getValue("prod.message"));
    }


    @Test
    public void testPrefixLen2() throws IOException, RecognitionException {
        PLexer lex = new PLexer(new ANTLRInputStream(TestParser.class.getResourceAsStream("/test_input_prefix_2.properties")));
        CommonTokenStream tokens = new CommonTokenStream(lex);

        PParser g = new PParser(tokens);
        g.start();

        ConfigTree config = g.getConfig();
        assertEquals("B", config.getValue("dev.test.message"));
        assertEquals("B", config.getValue("dev.*.message"));
        assertEquals("B", config.getValue("prod.*.message"));
        assertEquals("B", config.getValue("dev.*.message"));
    }
}
