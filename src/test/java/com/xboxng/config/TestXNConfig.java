/*
 *  Copyright Qiang Yu (qiangyu #### gmail.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.xboxng.config;

import com.xboxng.config.exception.XNConfigException;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TestXNConfig {
    @Test
    public void testHappyCase() throws XNConfigException, IOException {
        XNConfig config = new XNConfig(TestXNConfig.class.getResourceAsStream("/test_1.properties"));
        config.setProfileResolver(new ProfileResolver() {
            public String getProfile() {
                return "dev.xnconfig";
            }
        });
        Map credential = config.getMapValue("credential");

        assertEquals("dev_user", credential.get("username"));
        assertEquals(false, ((List) credential.get("options")).get(2)); // option_ssl

        Map map_has_map_has_map = config.getMapValue("map_has_map_has_map");
        Map credential_map = (Map) map_has_map_has_map.get("credential_map");
        Map map_has_map = (Map) credential_map.get("credential");
        credential_map = (Map) map_has_map.get("credential_map");
        Map credentail_map = (Map) credential_map.get("credential");

        assertEquals(credential, credentail_map);
    }


    @Test(expected = XNConfigException.class)
    public void testCyclicCase() throws XNConfigException, IOException {
        XNConfig config = new XNConfig(TestXNConfig.class.getResourceAsStream("/test_2.properties"));
        config.setProfileResolver(new ProfileResolver() {
            public String getProfile() {
                return "dev.xnconfig";
            }
        });
        Map credential = config.getMapValue("credential");

        assertEquals("dev_user", credential.get("username"));
        assertEquals(false, ((List) credential.get("options")).get(2)); // option_ssl

        Map map_has_map_has_map = config.getMapValue("map_has_map");
    }
}
